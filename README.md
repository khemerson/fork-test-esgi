# [ESGI] Automatisation de la virtualisation : Ansible

## Contexte
TP pour la matière Automatisation/Ansible pour l'école **ESGI** pour les classes **4SRC**
Ce playbook sert a éxécuter le playbook qui a été fait en 4SRC dans la matière "Automatisation de la virtualisation"

## Playbook
esgi.yml

## Rôles

- first_role
- facts
- apache2
- ntp
- ecole
- classe
- copytemplate

## Commande
**Lancer le playbook** 
```
ansible-playbook esgi.yml
```
**Editer un fichier cripté** 
```
ansible-vault edit global_vars/esgi.yml
```

## Remerciements
**Ecole**: ESGI - Kamal Hennou ainsi que les équipes administrative <br>
**Elèves**: merci à tous les élèves de 4SRC et GES executive 

## Liens
[Ecole](https://esgi.fr) **ESGI**<br>
[Filliare](https://esgi.fr/programmes/systeme-reseau-cloud-computing.html) **Système Réseau et Cloud computing** <br>
![ESGI](https://upload.wikimedia.org/wikipedia/commons/9/92/Esgi-logo.jpg)